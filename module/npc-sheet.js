/**
 * Implementation of the german RPG HeXXen 1733 (c) under the license of https://ulissesspiele.zendesk.com/hc/de/articles/360017969212-Inhaltsrichtlinien-f%C3%BCr-HeXXen-1733-Scriptorium.
 * Implementation based on the content of http://hexxen1733-regelwiki.de/
 * Author: Martin Brunninger
 * Software License: GNU GPLv3
 */

/**
 * Extend the basic ActorSheet with some very simple modifications
 * @extends {ActorSheet}
 */
 class NpcSheet extends HexxenActorSheet {

  /** @override */
	static get defaultOptions() {
	  return foundry.utils.mergeObject(super.defaultOptions, {
  	  classes: ["sheet", "actor", "npc"],
  	  template: `${Hexxen.basepath}/templates/npc-sheet.html.hbs`,
      width: 600,
      height: 600,
      tabs: [{navSelector: ".sheet-tabs", contentSelector: ".sheet-body", initial: "description"}]
    });
  }

  /* -------------------------------------------- */

  /** @override */
  async getData() {
    const out = await super.getData();
    // FIXME: Workaround 0.8.x
    out.data = out.data.data;
    return out;
  }

  /* -------------------------------------------- */

  // /** @override */
	// activateListeners(html) {
  //   super.activateListeners(html);
  //
  //   // Everything below here is only needed if the sheet is editable
  //   if (!this.options.editable) return;
  // }

  // /** @override */
  // async _updateObject(event, formData) {
  //
  //   // Update the Item
  //   return await super._updateObject(event, formData);
  // }
}
