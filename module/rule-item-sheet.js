﻿/**
 * Implementation of the german RPG HeXXen 1733 (c) under the license of https://ulissesspiele.zendesk.com/hc/de/articles/360017969212-Inhaltsrichtlinien-f%C3%BCr-HeXXen-1733-Scriptorium.
 * Implementation based on the content of http://hexxen1733-regelwiki.de/
 * Author: Martin Brunninger
 * Software License: GNU GPLv3
 */

/**
 * Extend the basic ItemSheet with some very simple modifications
 * @extends {ItemSheet}
 */
class RuleItemSheet extends HexxenItemSheet {

  constructor(...args) {
    super(...args);

    if (this.compendium) {
      // inject additional class to enable compendium-specific styles
      this.options.classes.push("compendium");
    } else if (!this.actor) {
      this.options.classes.push("world");
    }
  }

  /** @override */
	static get defaultOptions() {
	  return foundry.utils.mergeObject(super.defaultOptions, {
			classes: ["hexxen", "sheet", "rule"],
			template: `${Hexxen.basepath}/templates/rule-item-sheet.html`,
			width: 520,
			height: 450,
      tabs: [{navSelector: ".sheet-tabs", contentSelector: ".sheet-body", initial: "common"}]
      // TODO: Scrollbars
		});
  }

  /* -------------------------------------------- */

  get title() {
    // TODO: nach HexxenItemSheet verschieben??
    // TODO: localize
    // TODO: Markierung Compendium-Eintrag wäre ein Kandidat für ein TweakVTT Modul
    // Falls Tweak: this.actor ist nicht allgemein verfügbar!
    const prefix = this.compendium ? "[Compendium] " : (!this.actor ? "[World] " : "");
    return `${prefix}${super.title}`;
  }

  /* -------------------------------------------- */

  // // TODO: nach HexxenItemSheet verschieben
  // get actor() {
  //   return this.object.options.actor || null;
  // }

  get locType() {
    return this.localizeType(this.item.data.type);
  }

  localizeType(type) {
    if (!type) return type;

    if ("skill" === type) {
      type = "Fertigkeit";
    } else if ("general" === type) {
      type = "Allgemeine Fertigkeit";
    } else if ("weapon" === type) {
      type = "Waffenfertigkeit";
    } else if ("state" === type) {
      type = "Zustand";
    } else if ("internal_damage" === type) {
      type = "Innerer Schaden";
    } else if ("external_damage" === type) {
      type = "Äußerer Schaden";
    } else if ("penalty" === type) {
      type = "Malusschaden";
    } else if ("impediment" === type) {
      type = "Lähmungsschaden";
    } else if ("role" === type) {
      type = "Rolle";
    } else if ("power" === type) {
      type = "Jägerkraft";
    } else if ("jaeger" === type) {
      type = "Allgemeine Jägerkraft";
    } else if ("esprit" === type) {
      type = "Espritkraft";
    } else if ("ausbau" === type) {
      type = "Ausbaukraft";
    } else if ("geselle" === type) {
      type = "Geselleneffekt";
    } else if ("experte" === type) {
      type = "Experteneffekt";
    } else if ("meister" === type) {
      type = "Meistereffekt";
    } else if ("meisterprofession" === type) {
      type = "Meisterprofession";
    } else if ("npc-power" === type) {
      type = "NSC-Kraft";
    } else if ("regulation" === type) {
      type = "Regeltext";
    } else {
      type = type.capitalize();
    }
    return type;
  }

  // TODO: nach HexxenItemSheet verschieben
  get compendium() {
    return this.object.compendium || null;
  }

  get MARKER() { return {"stammeffekt": "S", "geselle": "G", "experte": "E", "meister": "M"}; }

  /** @override */
  async getData() {
    const out = await super.getData();
    // FIXME: Workaround
    const _oldItem = out.data;
    const _oldData = out.data.system;
    out.actor = this.actor;
    out.name = _oldData.name || _oldItem.name; // TODO: via Item?
    out.img = _oldItem.img; // TODO: basepath prüfen??
    out.type = this.localizeType(_oldItem.type); // motivation/role/profession/power
    if ("skill" === _oldItem.type && _oldItem.system.type) {
      out.type = this.localizeType(_oldItem.system.type); // allg./Waffenfertigkeit
    }
    if ("profession" === _oldItem.type && _oldItem.system.type) {
      out.type = this.localizeType(_oldItem.system.type); // meisterprofession
    }
    out.compendium = this.compendium;

    // TODO: besser ins Item? Dadurch auch für Markierung in Übersichtslisten nutzbar
    out.warnings = {};
    out.warnings.deprecated = this.item.getFlag(Hexxen.scope, "deprecated");
    out.warnings.bad = this.item.getFlag(Hexxen.scope, "badCompendiumId");
    out.warnings.update = this.item.getFlag(Hexxen.scope, "update");
    // const modified = this.item.getFlag(Hexxen.scope, "modified");
    if (!out.warnings.deprecated && !out.warnings.bad && !out.warnings.update) delete out.warnings;

    // _oldItem.type: motivation/role/profession/power
    // data.type: localized(_oldItem.type)
    // _oldData.type === _oldItem.data.type: jaeger/esprit/ausbau
    // _oldData.subtype === _oldItem.data.subtype: stammeffekt/geselle/experte/meister

    // origin data for sheet header
    out.origin = [];
    if ("power" !== _oldItem.type) {
      out.origin.push(out.type);
    } else {
      if (_oldData.subtype) {
        out.origin.push(this.localizeType(_oldData.subtype)); // stammeffekt/geselle/experte/meister
        out.origin.push(`${this.localizeType(_oldData.type)}: ${_oldData.origin.power?.name ?? ''}`);
      } else {
        out.origin.push(this.localizeType(_oldData.type)); // jaeger/esprit/ausbau
      }
      out.origin.push(_oldData.origin.name ?? '');

      out.hunterAbilityTypes = ['jaeger', 'esprit', 'ausbau', 'stammeffekt', 'geselle', 'experte', 'meister'].map(v => { return {key: v, label: this.localizeType(v)}; });
      out.hunterAbilityType = _oldData.type === "ausbau" ? _oldData.subtype || _oldData.type : _oldData.type;
      out.isHunterAbilityEffect = !! _oldData.subtype;
    }
    out.origin = out.origin.join(" / ");

    if (_oldData.create && out.locked){
      // TODO: besser im Template via helper umwandeln
      _oldData.create = await this._enrichHTML(_oldData.create);
    }

    // prepare powers and features
    if ( ["role", "profession"].includes(_oldItem.type) ) {
      this._preparePowers(out, _oldData.powers);
    }

    if ("power" === _oldItem.type && _oldData.features) {
      this._prepareFeatures(out, _oldData.features);
    }
    if ("skill" === _oldItem.type) {
      out.skillTypes = ['general', 'weapon'].map(v => { return {key: v, label: this.localizeType(v)}; });
      out.skillType = _oldData.type;
      const attributes = this.getAttributeDefinitions();
      out.attributes = ['SIN', 'WIS', 'WIL', 'KKR', 'ATH', 'GES'].map(v => { return {key: v, label: `${attributes[v].label} (${v})`}; });
      out.attribute = `${attributes[_oldData.attribute]?.label ?? ''} (${_oldData.attribute})`
    }
    if ("state" === _oldItem.type) {
      out.stateTypes = ['internal_damage', 'external_damage', 'penalty', 'impediment'].map(v => { return {key: v, label: this.localizeType(v)}; });
      out.stateType = _oldData.type;
    }

    _oldData.description = await this._enrichHTML(_oldData.description);
    _oldData.summary = await this._enrichHTML(_oldData.summary);

    out.data = _oldData;
    return out;
  }

  getAttributeDefinitions() {
    const attributes = game.model.Actor.character2e.attributes; // TODO Globale Definition erstellen
    // return Object.keys(attributes).map(key => { return {'key': key, 'label': attributes[key].label }; });
    return attributes;
  }

  _preparePowers(out, powers) {
    // TODO: Filter code für gelernte Kräfte gehört in den Actor
    const learned = this.actor ? this.actor.items.filter(i => i.type === "power") : [];
    out.powers = { "jaeger": [], "esprit": [], "ausbau": [] };

    powers.forEach(power => {
      // filter by type
      out.powers[power.type].push(power);
      power.uuid = `Compendium.${power.pack}.${power.id}`;

      // mark learned powers
      if (this.actor && learned.filter(i => i.system.name === power.name).length != 0) {
        power.learned = true;
        // TODO: +OwnedItemID
      }
      if ("ausbau" === power.type) {
        this._prepareFeatures(out, power.features);
      }
    });

    // sort filtered lists
    Object.values(out.powers).forEach(p => p.sort((a,b) => a.name.localeCompare(b.name)));
  }

  _prepareFeatures(out, features) {
    // FIXME: Workaround
    const _oldItem = out.data;
    // TODO: Filter code für gelernte Kräfte gehört in den Actor
    const learned = this.actor ? this.actor.items.filter(i => i.type === "power") : [];
    if ("power" === _oldItem.type) out.features = { "S": [], "G": [], "E": [], "M": [] };

    features.forEach(feature => {
      feature.marker = this.MARKER[feature.type];
      if (out.features) out.features[feature.marker].push(feature);
      feature.uuid = `Compendium.${feature.pack}.Item.${feature.id}`;


      if (this.actor && learned.filter(i => i.system.name === feature.name).length != 0) {
        feature.learned = true;
        // TODO: +OwnedItemID
      }
      // TODO: Voraussetzung prüfen
      if (false) {
        feature.locked = true;
      }
    });
}

  /* -------------------------------------------- */

  /** @override */
	activateListeners(html) {
    super.activateListeners(html);

    // html.find("a[data-action='open']").on("click", HexxenCompendiumHelper.onClickOpenPower);
    html.find(".entity-link").on("click", (event => {
      if (!this.actor || !this.actor.items) return;

      // TODO: via OwnedItemID identifizieren
      // TODO: vgl. mit flags.core.sourceId; Problem(?): bei Import geht CompendiumId trotzdem verloren.
      const pack = event.target.dataset.pack;
      const lookup = event.target.dataset.lookup || event.target.dataset.id; // FIXME: warum ist lookup teilweise in id umgeschrieben?
      const entity = this.actor.items.filter(i => {
        const p = i.getFlag("hexxen-1733", "compendium.pack") === pack;
        const id = i.getFlag("hexxen-1733", "compendium.id") === lookup;
        const n = i.getFlag("hexxen-1733", "compendium.name") === lookup;
        return (p && (id || n));
      });

      if (entity.length) {
        const app = entity[0].sheet;
        app.render(true);
        HexxenAppAlignmentHelper.align(app, event);
        event.preventDefault();
        event.stopImmediatePropagation();
      }
    }).bind(this));

    // Everything below here is only needed if the sheet is editable
    if (!this.options.editable) return;
  }

  /* -------------------------------------------- */

  /** @override */
  async _updateObject(event, formData) {

    // TODO: img ändern, falls erlaubt

    // TODO: rewrite von Array Inhalt Änderungen automatisieren
    if (formData['data.references.0.source']) {
      const org = foundry.utils.duplicate(this.item.system.references);
      org[0].source = formData['data.references.0.source'];
      formData['data.references'] = org;
      delete formData['data.references.0.source'];
    }

    const originType = formData['origin.type'];
    if (originType) {
      delete formData['origin.type'];
      if (['', 'jaeger', 'esprit', 'ausbau'].includes(originType)) {
        formData['system.type'] = originType;
        formData['system.subtype'] = null;
        formData['system.origin.-=power'] = null;
      }
      else {
        formData['system.type'] = 'ausbau';
        formData['system.subtype'] = originType;
      }
    }

    // Update the Item
    return await super._updateObject(event, formData);
  }
}
