/**
 * Implementation of the german RPG HeXXen 1733 (c) under the license of https://ulissesspiele.zendesk.com/hc/de/articles/360017969212-Inhaltsrichtlinien-f%C3%BCr-HeXXen-1733-Scriptorium.
 * Implementation based on the content of http://hexxen1733-regelwiki.de/
 * Author: Martin Brunninger
 * Software License: GNU GPLv3
 */


class HexxenItemSheet extends HexxenSheetMixin(ItemSheet) {

  get editMode() {
    return this.actor?.getFlag(Hexxen.scope, "editMode") || false;
  }

  get custom() {
    return this.item.getFlag(Hexxen.scope, "custom") || false;
  }

  /** @override */
  async getData() {
    const data = await super.getData();
    data.custom = this.custom;
    data.editMode = this.editMode;
    data.locked = this.item?.parent ? (!data.editMode || !data.custom) : !data.editable;
    // FIXME: Achtung: foundry verwendet auch locked, falls nicht editierbar (editable)
    data.cssClass = ((data.cssClass || '') + (data.locked ? ' locked' : '')).trim();

    return data;
  }
}


/**
 * Extend the basic ItemSheet with some very simple modifications
 * @extends {ItemSheet}
 */
class SimpleItemSheet extends HexxenSheetMixin(ItemSheet) {

  /** @override */
  static get defaultOptions() {
    return foundry.utils.mergeObject(super.defaultOptions, {
      classes: ["worldbuilding", "sheet", "item"],
      template: `${Hexxen.basepath}/templates/item-sheet.html`,
      width: 520,
      height: 480,
      tabs: [{ navSelector: ".sheet-tabs", contentSelector: ".sheet-body", initial: "description" }]
    });
  }

  /* -------------------------------------------- */

  /** @override */
  async getData() {
    const out = await super.getData();
    out.data = out.data.system; // WORKAROUND für Strukturänderung 0.7.x --> 0.8.x

    out.data.description = await this._enrichHTML(out.data.description);

    return out;
  }

  /* -------------------------------------------- */

  // /** @override */
  // activateListeners(html) {
  //   super.activateListeners(html);
  //
  //   // Everything below here is only needed if the sheet is editable
  //   if (!this.options.editable) return;
  // }

  // /** @override */
  // async _updateObject(event, formData) {
  //
  //   // Update the Item
  //   return await super._updateObject(event, formData);
  // }
}
